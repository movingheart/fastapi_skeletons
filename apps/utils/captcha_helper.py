#!/usr/bin/evn python
# coding=utf-8
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +
#        ┏┓　　　┏┓+ +
# 　　　┏┛┻━━━┛┻┓ + +
# 　　　┃　　　　　　 ┃ 　
# 　　　┃　　　━　　　┃ ++ + + +
# 　　 ████━████ ┃+
# 　　　┃　　　　　　 ┃ +
# 　　　┃　　　┻　　　┃
# 　　　┃　　　　　　 ┃ + +
# 　　　┗━┓　　　┏━┛
# 　　　　　┃　　　┃　　　　　　　　　　　
# 　　　　　┃　　　┃ + + + +
# 　　　　　┃　　　┃　　　　Codes are far away from bugs with the animal protecting　　　
# 　　　　　┃　　　┃ + 　　　　神兽保佑,代码无bug　　
# 　　　　　┃　　　┃
# 　　　　　┃　　　┃　　+　　　　　　　　　
# 　　　　　┃　 　　┗━━━┓ + +
# 　　　　　┃ 　　　　　　　┣┓
# 　　　　　┃ 　　　　　　　┏┛
# 　　　　　┗┓┓┏━┳┓┏┛ + + + +
# 　　　　　　┃┫┫　┃┫┫
# 　　　　　　┗┻┛　┗┻┛+ + + +
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +"""
"""
# 版权说明
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +

# @Time  : 2020/4/21 13:07

# @Author : mayn

# @Project : ZFlask

# @FileName: captcha_helper.py

# @Software: PyCharm

# 作者：小钟同学

# 著作权归作者所有

# 文件功能描述: 图片验证码 不使用第三方插件库的情况下
"""

import random
import string

"""
Image: 画布
ImageDraw: 画笔
ImageFont: 字体
"""
from PIL import Image, ImageDraw, ImageFont


class Captcha(object):
    # 生成的验证码的个数
    number = 4
    # 图片的宽度和高度
    size = (90, 38)
    # 字体大小
    fontsize = 35
    # 干扰线条数
    line_number = 3

    SOURCE = list(string.ascii_letters)
    SOURCE.extend(map(str, list(range(0, 2))))

    @classmethod
    def __gen_line(cls, draw, width, height):
        """
        绘制干扰线
        """
        begin = (random.randint(0, width), random.randint(0, height))
        end = (random.randint(0, width), random.randint(0, height))
        draw.line([begin, end], fill=cls.__gen_random_color(), width=2)

    @classmethod
    def __gen_random_color(cls, start=0, end=255):
        """
        产生随机颜色
        颜色的取值范围是0~255
        """
        random.seed()
        return (
            random.randint(start, end),
            random.randint(start, end),
            random.randint(start, end),
        )

    @classmethod
    def __gen_points(cls, draw, point_chance, width, height):
        """
        绘制干扰点
        """
        chance = min(100, max(0, int(point_chance)))
        for w in range(width):
            for h in range(height):
                temp = random.randint(0, 100)
                if temp > 100 - chance:
                    draw.point((w, h), fill=cls.__gen_random_color())

    @classmethod
    def __gen_random_font(cls):
        """
        采用随机字体
        :return:
        """
        # /usr/share/fonts/dejavu/DejaVuSansCondensed-Oblique.ttf
        # /usr/share/fonts/dejavu/DejaVuSansCondensed-Bold.ttf
        # /usr/share/fonts/dejavu/DejaVuSans-Bold.ttf
        fonts = ["consola.ttf", "consolab.ttf", "consolai.ttf"]
        font = random.choice(fonts)
        return "helper/captcha/" + font

    @classmethod
    def gen_text(cls, number):
        """
        随机生成一个字符串
        :param number: 字符串数量
        """
        return "".join(random.sample(cls.SOURCE, number))

    @classmethod
    def gen_graph_captcha(cls):
        width, height = cls.size
        # A表示透明度
        image = Image.new("RGBA", (width, height), cls.__gen_random_color(0, 100))
        # 字体
        font = ImageFont.truetype(cls.__gen_random_font(), cls.fontsize)
        # 创建画笔
        draw = ImageDraw.Draw(image)
        # 生成随机字符串
        text = cls.gen_text(cls.number)
        # 字体大小
        font_width, font_height = font.getsize(text)
        # 填充字符串
        draw.text(
            ((width - font_width) / 2, (height - font_height) / 2),
            text,
            font=font,
            fill=cls.__gen_random_color(150, 255),
        )
        # 绘制干扰线
        for x in range(0, cls.line_number):
            cls.__gen_line(draw, width, height)
        # 绘制噪点
        cls.__gen_points(draw, 10, width, height)

        return text, image


if __name__ == '__main__':
    from io import BytesIO
    from flask import Blueprint, request, make_response, session


    # @route_admin.route('/Captcha', methods=['GET'])
    def GetCaptcha():
        text, image = Captcha().gen_graph_captcha()
        out = BytesIO()
        image.save(out, 'png')
        out.seek(0)
        resp = make_response(out.read())
        resp.content_type = 'image/png'
        # 存入session
        # session['Captcha'] = text
        return resp
